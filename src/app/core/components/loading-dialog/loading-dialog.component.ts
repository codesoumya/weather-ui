import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  message: string;
}

@Component({
  selector: 'app-loading-dialog',
  templateUrl: './loading-dialog.component.html',
  styleUrls: ['./loading-dialog.component.scss']
})
export class LoadingDialogComponent implements OnInit {
  messageTxt = 'Please Wait While We\nComplete Your Request...';

  constructor(@Inject(MAT_DIALOG_DATA) private data: DialogData) {}

  ngOnInit(): void {
    /**
     * Check if message text is provided
     * then assign it to messageTxt
     */
    if (this.data.message) {
      this.messageTxt = this.data.message;
    }
  }
}
