import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { LoadingDialogComponent } from './loading-dialog.component';

function spyPropertyGetter<T, K extends keyof T>(
  spyObj: jasmine.SpyObj<T>,
  propName: K
): jasmine.Spy<() => T[K]> {
  return Object.getOwnPropertyDescriptor(spyObj, propName)?.get as jasmine.Spy<
    () => T[K]
  >;
}

describe('LoadingDialogComponent', () => {
  let component: LoadingDialogComponent;
  let fixture: ComponentFixture<LoadingDialogComponent>;
  const mockMatDialog = jasmine.createSpyObj([], {
    message: ''
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoadingDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: MAT_DIALOG_DATA, useValue: mockMatDialog }]
    }).compileComponents();
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(LoadingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    tick();
  }));

  it('should create loading dialog component!', () => {
    expect(component).toBeTruthy();
  });

  it('should render message data!', () => {
    spyPropertyGetter(mockMatDialog, 'message').and.returnValue('Test message');
    component.ngOnInit();
    expect(component.messageTxt).toBe('Test message');
  });
});
