import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoadingDialogComponent } from '../components/loading-dialog/loading-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private _loading = new BehaviorSubject<boolean>(true);

  get loading(): Observable<boolean> {
    return this._loading.asObservable();
  }

  constructor(public dialog: MatDialog) {}

  /**
   * Open material dialog component
   * @param message is string type and optional
   * @returns MatDialogReference of dialog model
   */
  openLoadingDialog(
    message?: string
  ): MatDialogRef<LoadingDialogComponent, { message: string }> {
    return this.dialog.open(LoadingDialogComponent, {
      data: { message: message }
    });
  }

  /**
   * Tigger next()
   * @param isLoading is boolean type and required
   */
  toggleLoading(isLoading: boolean): void {
    this._loading.next(isLoading);
  }

  /**
   * Remove the decimal place value from a number
   * @param temp is number type and required
   * @returns truncated value
   */
  truncate(temp: number): number {
    return Math.trunc(temp);
  }
}
