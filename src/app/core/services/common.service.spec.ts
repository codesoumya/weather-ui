import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';

import { CommonService } from './common.service';

describe('CommonService', () => {
  let service: CommonService;
  const mockMatDialog = jasmine.createSpyObj(['open']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
      providers: [{ provide: MatDialog, useValue: mockMatDialog }]
    });
    service = TestBed.inject(CommonService);
  });

  it('should be created (common service)', () => {
    expect(service).toBeTruthy();
  });

  it('should return loading observable!', () => {
    service.loading.subscribe(value => {
      expect(value).toBe(true);
    });
  });

  it('should call dialog open method!', () => {
    service.openLoadingDialog();
    expect(service.dialog.open).toHaveBeenCalled();
  });

  it('should next the _loading BehaviorSubject!', () => {
    service.toggleLoading(false);
    service.loading.subscribe(value => {
      expect(value).toBe(false);
    });
  });

  it('should return truncated value!', () => {
    service.truncate(24.58);
    expect(service.truncate(24.58)).toBe(24);
  });
});
