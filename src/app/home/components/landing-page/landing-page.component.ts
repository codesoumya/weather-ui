import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import {
  CustomRes,
  LandingPageService
} from '../../services/landing-page.service';
import { CommonService } from '../../../core/services/common.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  isLoading!: Observable<boolean>;
  weatherData!: Observable<CustomRes[]>;

  constructor(
    private landingService: LandingPageService,
    private commonService: CommonService
  ) {}

  /**
   * Life cycle method for initialization
   * Assigning isLoading to CommonService get loading property
   * Assigning weatherData to LandingPageService getWeatherDate() method
   * isLoading and weatherData both are observable and subscribed via async pipe in template
   */
  ngOnInit(): void {
    this.isLoading = this.commonService.loading;
    this.weatherData = this.landingService.getWeatherDate();
  }
}
