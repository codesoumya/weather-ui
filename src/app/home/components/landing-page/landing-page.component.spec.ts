import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';
import { of } from 'rxjs';

import { CommonService } from 'src/app/core/services/common.service';
import { LandingPageService } from '../../services/landing-page.service';
import { LandingPageComponent } from './landing-page.component';

describe('LandingPageComponent', () => {
  let component: LandingPageComponent;
  let fixture: ComponentFixture<LandingPageComponent>;
  const mockLandingService = jasmine.createSpyObj('LandingPageService', [
    'getWeatherDate'
  ]);
  const mockCommonService = {
    get loading() {
      return of(true);
    }
  };
  let response: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LandingPageComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: LandingPageService, useValue: mockLandingService },
        { provide: CommonService, useValue: mockCommonService }
      ]
    }).compileComponents();
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(LandingPageComponent);
    component = fixture.componentInstance;
    mockLandingService.getWeatherDate.and.returnValue(of(response));
    fixture.detectChanges();
    tick();
    // await fixture.whenStable();
  }));

  it('should create landing page component', () => {
    expect(component).toBeTruthy();
  });
});
