import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { of } from 'rxjs';

import { CommonService } from '../../../core/services/common.service';
import { CityForecastService } from '../../services/city-forecast.service';
import { CityForecastComponent } from './city-forecast.component';

describe('CityForecastComponent', () => {
  let component: CityForecastComponent;
  let fixture: ComponentFixture<CityForecastComponent>;
  const routerSpy = { navigate: jasmine.createSpy('navigate') };
  const mockForecastService = jasmine.createSpyObj('CityForecastService', [
    'getForcastByCity'
  ]);
  const mockCommonService = {
    get loading() {
      return of(true);
    }
  };
  let router: Router;
  let response: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CityForecastComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of({
              get: (key: string) => {
                return 'Barcelona,ES';
              },
              has: (key: string) => {
                return true;
              }
            })
          }
        },
        { provide: CityForecastService, useValue: mockForecastService },
        { provide: CommonService, useValue: mockCommonService }
      ]
    }).compileComponents();
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(CityForecastComponent);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
    mockForecastService.getForcastByCity.and.returnValue(of(response));
    fixture.detectChanges();
    // await fixture.whenStable();
    tick();
  }));

  it('should create city forecast component!', () => {
    expect(component).toBeTruthy();
  });

  it('should city name equal to <Barcelona,ES>', () => {
    component.ngOnInit();
    expect(component.cityName).toEqual('Barcelona,ES');
  });
});
