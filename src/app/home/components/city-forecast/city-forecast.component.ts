import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

import {
  CityForecastService,
  CustForeRes
} from '../../services/city-forecast.service';
import { CommonService } from '../../../core/services/common.service';

@Component({
  selector: 'app-city-forecast',
  templateUrl: './city-forecast.component.html',
  styleUrls: ['./city-forecast.component.scss']
})
export class CityForecastComponent implements OnInit {
  isLoading!: Observable<boolean>;
  cityName!: string;
  forecast!: Observable<CustForeRes[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private forecastService: CityForecastService,
    private commonService: CommonService
  ) {}

  /**
   * Life cycle method for Initialization
   * Assigning isLoading observable to CommonService get loading property
   * Subscribe to the paramMap to get city name from the URL
   * Check param has id or not (though we always have id)
   * If we param id present assigning cityname and forecast
   * isLoading and forecast both are observable and subscribed via async pipe in template
   */
  ngOnInit(): void {
    this.isLoading = this.commonService.loading;
    this.route.paramMap.subscribe(param => {
      if (!param.has('id')) {
        this.router.navigate(['/home/landing-page']);
        return;
      }
      this.cityName = param.get('id') as string;
      this.forecast = this.forecastService.getForcastByCity(
        param.get('id') as string
      );
    });
  }
}
