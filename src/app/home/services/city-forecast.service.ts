import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

import { CommonService } from '../../core/services/common.service';
import { environment } from '../../../environments/environment';

export interface ForecastRes {
  list: List[];
}
export interface List {
  dt_txt: string;
  main: Main;
}
export interface Main {
  sea_level: number;
  humidity: number;
  temp: number;
}
export interface CustForeRes {
  date: string;
  seaLevel: number;
  temp: number;
  humidity: number;
}

@Injectable({
  providedIn: 'root'
})
export class CityForecastService {
  constructor(
    private http: HttpClient,
    private commonService: CommonService,
    private _snackBar: MatSnackBar
  ) {}

  /**
   * Opening Loading dialog and close it once done
   * Http get call to fetch forecast data form open weather endpoint
   * Manipulating observalbe before subscribe with rxjs operators
   * Like: Filter the original 09:00 time forecast,
   * modify response type according to the need,
   * catchError and show the snack bar message and close dialog and loader
   * @param cityName is string type and it is required
   * @returns observable with type CustForeRes[]
   */
  getForcastByCity(cityName: string): Observable<CustForeRes[]> {
    const loadingRef = this.commonService.openLoadingDialog();
    return this.http
      .get<ForecastRes>(`${environment.apiEndPoint}/forecast`, {
        params: { q: cityName, units: 'metric', appid: environment.appId }
      })
      .pipe(
        map((chunk: ForecastRes) => {
          const filteredData = chunk.list.filter(
            item => item.dt_txt.split(' ')[1] === '09:00:00'
          );
          return filteredData.map<CustForeRes>((data: List) => {
            loadingRef.close();
            this.commonService.toggleLoading(false);
            return {
              date: data.dt_txt.split(' ')[0],
              temp: this.commonService.truncate(data.main.temp),
              seaLevel: data.main.sea_level,
              humidity: data.main.humidity
            };
          });
        }),
        catchError(error => {
          loadingRef.close();
          this.commonService.toggleLoading(false);
          this._snackBar.open(
            'Something went wrong! Please try again later.',
            'Close',
            {
              duration: 5000
            }
          );
          return throwError(error);
        })
      );
  }
}
