import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatSnackBar } from '@angular/material/snack-bar';

import { CityForecastService } from './city-forecast.service';
import { CommonService } from '../../core/services/common.service';
import { of, throwError } from 'rxjs';

describe('CityForecastService', () => {
  let service: CityForecastService;
  const mockHttp = jasmine.createSpyObj(['get', 'post', 'put', 'delete']);
  const mockCommonService = jasmine.createSpyObj('CommonService', [
    'openLoadingDialog',
    'toggleLoading',
    'truncate'
  ]);
  const mockMatSnackBar = jasmine.createSpyObj(['open']);
  const dialogRef = jasmine.createSpyObj('MatDialogRef', ['close']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: HttpClient, useValue: mockHttp },
        { provide: CommonService, useValue: mockCommonService },
        { provide: MatSnackBar, useValue: mockMatSnackBar }
      ]
    });
    service = TestBed.inject(CityForecastService);
  });

  it('should be created (city forecast service)', () => {
    expect(service).toBeTruthy();
  });

  it('should call getForcastByCity method!', fakeAsync(() => {
    mockCommonService.openLoadingDialog.and.returnValue(dialogRef);
    mockCommonService.toggleLoading.and.returnValue(false);
    mockCommonService.truncate.and.returnValue(25);
    const mock = {
      list: [
        {
          dt_txt: '2021-06-19 09:00:00',
          main: {
            temp: 25.36,
            sea_level: 0,
            humidity: 0
          }
        }
      ]
    };
    const mockModify = {
      date: '2021-06-19',
      seaLevel: 0,
      temp: 25,
      humidity: 0
    };
    mockHttp.get.and.returnValue(of(mock));
    service.getForcastByCity('any').subscribe(data => {
      expect(data).toEqual([mockModify]);
    });
    tick();
  }));

  it('should catch error of getForcastByCity method!', fakeAsync(() => {
    mockCommonService.openLoadingDialog.and.returnValue(dialogRef);
    mockCommonService.toggleLoading.and.returnValue(false);
    mockHttp.get.and.returnValue(throwError('any'));
    service.getForcastByCity('any').subscribe(
      data => expect(data).toBeNull(),
      error => expect(error).toBe('any')
    );
    tick();
  }));
});
