import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatSnackBar } from '@angular/material/snack-bar';

import { LandingPageService } from './landing-page.service';
import { CommonService } from '../../core/services/common.service';
import { of, throwError } from 'rxjs';

describe('LandingPageService', () => {
  let service: LandingPageService;
  const mockHttp = jasmine.createSpyObj(['get', 'post', 'put', 'delete']);
  const mockCommonService = jasmine.createSpyObj('CommonService', [
    'openLoadingDialog',
    'toggleLoading',
    'truncate'
  ]);
  const mockMatSnackBar = jasmine.createSpyObj(['open']);
  const dialogRef = jasmine.createSpyObj('MatDialogRef', ['close']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: HttpClient, useValue: mockHttp },
        { provide: CommonService, useValue: mockCommonService },
        { provide: MatSnackBar, useValue: mockMatSnackBar }
      ]
    });
    service = TestBed.inject(LandingPageService);
  });

  it('should be created (landing page service)', () => {
    expect(service).toBeTruthy();
  });

  it('should call getCurrentWeatherByCity method!', fakeAsync(() => {
    mockCommonService.openLoadingDialog.and.returnValue(dialogRef);
    mockCommonService.toggleLoading.and.returnValue(false);
    mockCommonService.truncate.and.returnValue(25);
    const mock = {
      name: 'any',
      sys: {
        country: 'any',
        sunrise: 0,
        sunset: 0
      },
      main: {
        temp: 0
      }
    };
    const mockModify = {
      cityName: 'any',
      countryName: 'any',
      temp: 25,
      sunrise: 0,
      sunset: 0
    };
    mockHttp.get.and.returnValue(of(mock));
    service.getCurrentWeatherByCity('any').subscribe(data => {
      expect(data).toEqual(mockModify);
    });
    tick();
  }));

  it('should call getWeatherDate method!', fakeAsync(() => {
    mockCommonService.openLoadingDialog.and.returnValue(dialogRef);
    mockCommonService.toggleLoading.and.returnValue(false);
    mockCommonService.truncate.and.returnValue(25);
    const mock = {
      name: 'any',
      sys: {
        country: 'any',
        sunrise: 0,
        sunset: 0
      },
      main: {
        temp: 0
      }
    };
    const mockModify = {
      cityName: 'any',
      countryName: 'any',
      temp: 25,
      sunrise: 0,
      sunset: 0
    };
    mockHttp.get.and.returnValue(of(mock));
    spyOn(service, 'getCurrentWeatherByCity').and.returnValue(of(mockModify));
    service.getWeatherDate().subscribe(data => {
      expect(data).toEqual([
        mockModify,
        mockModify,
        mockModify,
        mockModify,
        mockModify
      ]);
    });
    tick();
  }));

  it('should catch error of getWeatherDate method!', fakeAsync(() => {
    mockCommonService.openLoadingDialog.and.returnValue(dialogRef);
    mockCommonService.toggleLoading.and.returnValue(false);
    spyOn(service, 'getCurrentWeatherByCity').and.returnValue(
      throwError('any')
    );
    service.getWeatherDate().subscribe(
      data => expect(data).toBeNull(),
      error => expect(error).toBe('any')
    );
    tick();
  }));
});
