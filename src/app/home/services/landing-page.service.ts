import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, forkJoin, throwError } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

import { CommonService } from '../../core/services/common.service';
import { environment } from '../../../environments/environment';

export interface WeatherRes {
  name: string;
  sys: Sys;
  main: Main;
}
export interface Main {
  temp: number;
}
export interface Sys {
  country: string;
  sunrise: number;
  sunset: number;
}
export interface CustomRes {
  cityName: string;
  countryName: string;
  temp: number;
  sunrise: number;
  sunset: number;
}

@Injectable({
  providedIn: 'root'
})
export class LandingPageService {
  private _cityList = ['Florence', 'Istanbul', 'Rome', 'Barcelona', 'Lisbon'];

  constructor(
    private http: HttpClient,
    private commonService: CommonService,
    private _snackBar: MatSnackBar
  ) {}

  /**
   * Http get call to fetch current weather data form open weather endpoint
   * Manipulating observalbe before subscribe with rxjs operators
   * Like: modify response type according to the need
   * @param cityName is string type and required
   * @returns observable with type CustForeRes
   */
  getCurrentWeatherByCity(cityName: string): Observable<CustomRes> {
    return this.http
      .get<WeatherRes>(`${environment.apiEndPoint}/weather`, {
        params: { q: cityName, units: 'metric', appid: environment.appId }
      })
      .pipe(
        map((data: WeatherRes) => {
          return {
            cityName: data.name,
            countryName: data.sys.country,
            temp: this.commonService.truncate(data.main.temp),
            sunrise: data.sys.sunrise,
            sunset: data.sys.sunset
          };
        })
      );
  }

  /**
   * Opening Loading dialog and close it once done
   * Gathering all 5 cities current weather date as array of observable
   * Fetch all respone at the same time with forkJoin
   * Manipulating observalbe before subscribe with rxjs operators
   * Like: close dialog and loader,
   * catchError and show the snack bar message and close dialog and loader
   * @returns observable with type CustForeRes[]
   */
  getWeatherDate(): Observable<CustomRes[]> {
    const loadingRef = this.commonService.openLoadingDialog();
    return forkJoin(
      this._cityList.map((item: string) => {
        return this.getCurrentWeatherByCity(item);
      })
    ).pipe(
      tap(data => {
        loadingRef.close();
        this.commonService.toggleLoading(false);
        return data;
      }),
      catchError(error => {
        loadingRef.close();
        this.commonService.toggleLoading(false);
        this._snackBar.open(
          'Something went wrong! Please try again later.',
          'Close',
          {
            duration: 5000
          }
        );
        return throwError(error);
      })
    );
  }
}
