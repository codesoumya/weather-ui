import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityForecastComponent } from './components/city-forecast/city-forecast.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';

const routes: Routes = [
  {
    path: 'landing-page',
    component: LandingPageComponent
  },
  {
    path: 'forecast/:id',
    component: CityForecastComponent
  },
  {
    path: '',
    redirectTo: 'landing-page',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
